# @universis/candidates

Universis api server plugin for study program candidates, internship selection etc

## Installation

npm i @universis/candidates

## Usage

Register `CandidateService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/candidates#CandidateService"
        }
    ]

Add `CandidateSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/candidates#CandidateSchemaLoader"
                    }
                ]
            }
        }
    }

# Universis Candidates

Universis api server plugin for study program candidates, internship selection etc

##### Installation

    npm i @universis/candidates
    
 `@universis/candidates` plugin extends api server for managing candidate requests
 
 #### Service Configuration
 
 Register CandidateService as [Universis Api Server](https://gitlab.com/universis/universis) service  
 
 server/config/app.production.json
 
     "services": [
             ...
             {
                 "serviceType": "@universis/candidates#CandidateService"
             },
             ...
         ]


 ### Development

##### Clone universis-api server, install dependencies and build

        git clone https://gitlab.com/universis/universis-api.git
        cd universis-api
        npm i
        npm run build
        
Note: Following instructions provided at [https://gitlab.com/universis/universis-api/-/blob/master/INSTALL.md](https://gitlab.com/universis/universis-api/-/blob/master/INSTALL.md) for configuring universis-api server

#####  Clone project and install dependencies

       git clone https://gitlab.com/universis/universis-candidates.git
       cd universis-candidates
       npm i
       
##### Install universis-api as development dependency (for testing)

       npm i ../universis-api --save-dev --no-save
       
##### Test plugin

        npm test
 
 
###### Features

Universis api server plugin for candidates have the following features:



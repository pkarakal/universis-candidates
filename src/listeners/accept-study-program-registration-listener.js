import { DataObjectState, FunctionContext, DataCacheStrategy } from "@themost/data";
import { DataError, AccessDeniedError, DataNotFoundError } from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const context = event.model.context;
    // get current status
    const actionStatus = await context.model('StudyProgramRegisterAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();

    if (actionStatus !== 'CompletedActionStatus') {
        return;
    }
    if (event.state === DataObjectState.Insert) {
        throw new AccessDeniedError('A study program registration cannot be completed automatically.', null);
    }
    if (event.state === DataObjectState.Update) {
        // get previous status
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (previousStatus === 'ActiveActionStatus') {
                // create student

            const item = await context.model('StudyProgramRegisterAction')
                .where('id').equal(event.target.id)
                .expand('studyProgram', {
                    name: 'candidate',
                    options: {
                        $expand: 'person'
                    }
                })
                .silent().getItem();
                if (item == null) {
                    throw new DataNotFoundError('Current item cannot be found or is inaccessible', null, 'StudyProgramRegisterAction');
                }
                // get study program data
                const studyProgramSpecialty = await context.model('StudyProgramSpecialty')
                    .where('studyProgram').equal(item.studyProgram).and('specialty').equal(-1).getItem();
                // create person
                const person = Object.assign({}, item.candidate.person);
                // remove person id in order to insert a person based on its data
                delete person.id;
                const newPerson = await context.model('Person').save(person);

                // create student
                // important note: each student has a unique for each study program
                const newStudent = Object.assign({}, item.candidate);
                delete newStudent.id;
                newStudent.person = newPerson;
                newStudent.studyProgramSpecialty = studyProgramSpecialty;
                newStudent.semester = 1;
                newStudent.inscriptionDate = new Date();
                newStudent.inscriptionSemester = 1;

                const Students = context.model('Student');
                const student = await Students.save(newStudent);
                // add user to students group
                const userGroups = context.model('User').convert(item.owner).property('groups');
                // add to students group
                await userGroups.silent().insert({
                    name: 'Students'
                });
                // update candidateStudent with new student id
                item.candidate.student = student.id;newStudent
                // save candidateStudent
                await context.model('CandidateStudent').save(item.candidate);
                // // and remove it from candidates
                // await userGroups.silent().remove({
                //     name: 'Candidates'
                // });
                const cache = context.getApplication().getConfiguration().getStrategy(DataCacheStrategy);
                if (cache != null) {
                    cache.clear();
                }

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

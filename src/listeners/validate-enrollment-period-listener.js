import { DataError, DataNotFoundError, AccessDeniedError } from '@themost/common';

function validateEnrollmentPeriod(enrollmentPeriod) {
    const now = new Date();
    let valid = false;
    if (enrollmentPeriod.validFrom instanceof Date) {
        if (enrollmentPeriod.validThrough instanceof Date) {
            valid = enrollmentPeriod.validFrom <= now && enrollmentPeriod.validThrough > now;
        } else {
            valid = enrollmentPeriod.validFrom < now;
        }
    } else if (enrollmentPeriod.validThrough instanceof Date) {
        valid = enrollmentPeriod.validThrough > now;
    }
    return valid;
}
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    // check if current user is the owner of this object
    let owner;
    let enrollmentPeriod;
    const context = event.model.context;
    if (event.state === 2) {
        if (Object.prototype.hasOwnProperty.call(event.target, 'owner')) {
            // get owner
            owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        } else {
            owner = await context.model('User').find(event.previous.owner).select('name').silent().value();
        }
        enrollmentPeriod = await context.model('StudyProgramEnrollmentEvent')
            .where('studyProgram').equal(event.previous.studyProgram)
            .and('inscriptionYear').equal(event.previous.inscriptionYear)
            .and('inscriptionPeriod').equal(event.previous.inscriptionPeriod)
            .silent()
            .getItem();
    } else if (event.state === 1) {
        // get owner
        owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        enrollmentPeriod = await context.model('StudyProgramEnrollmentEvent')
            .where('studyProgram').equal(event.target.studyProgram)
            .and('inscriptionYear').equal(event.target.inscriptionYear)
            .and('inscriptionPeriod').equal(event.target.inscriptionPeriod)
            .silent()
            .getItem();
        if (enrollmentPeriod) {
            // set enrollment event
            event.target.studyProgramEnrollmentEvent = enrollmentPeriod.id;
        }
    }
    if (owner == null) {
        throw new DataNotFoundError('Action owner cannot be found or is inaccessible', null, event.model.name, event.target.id);
    }
    if (owner === context.user.name) {
        // validate period
        if (enrollmentPeriod == null) {
            throw Object.assign(new DataError('ERR_MISSING_PERIOD', 'The enrollment period of the specified study program cannot be found', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
        const valid = validateEnrollmentPeriod(enrollmentPeriod);
        if (valid === false) {
            throw Object.assign(new DataError('ERR_EXPIRED_PERIOD', 'The enrollment period has been expired', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
